﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace MabadumbaPlayer
{
    public partial class Mabadumba : Form
    {

        MusicPlayerClass MabadumbaPlayer = new MusicPlayerClass();        

        bool state_buttonShuffle = false;
        bool state_buttonLoop = false;

        bool state_songPlaying = false;

        string str = "";

        //////////////////////////////
        //////////////////////////////

        public Mabadumba()
        {
            InitializeComponent();

            DirectoryInfo d = new DirectoryInfo("C:\\Users\\KaaN\\Desktop\\müzik");// Going to file path
            textBox_URL.Text = "C:\\Users\\KaaN\\Desktop\\müzik";

            Thread threadStatus = new Thread(new ThreadStart(checkStatus));

            threadStatus.Start();

            button_Play.Enabled = false;

        }

        //////////////////////////////
        //////////////////////////////
        
        private void button_SelectURL_Click(object sender, EventArgs e)
        {
     
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    DirectoryInfo d = new DirectoryInfo(fbd.SelectedPath);// Going to file path

                    textBox_URL.Text = fbd.SelectedPath;

                    FileInfo[] Files = d.GetFiles("*.mp3"); // Getting mp3 files

                    // listbox temizleniyor
                    listBox_Song.Items.Clear();

                    MabadumbaPlayer.stop();

                    foreach (FileInfo file in Files)
                    {
                        str = file.Name;
                        listBox_Song.Items.Add(str);
                        str = "";
                    }
                }
            }
        }

        private void button_OpenURL_Click(object sender, EventArgs e)
        {
            DirectoryInfo d = new DirectoryInfo(textBox_URL.Text);// Going to file path

            FileInfo[] Files = d.GetFiles("*.mp3"); // Getting mp3 files

            foreach (FileInfo file in Files)
            {
                str = file.Name;
                listBox_Song.Items.Add(str);
                str = "";
            }

            button_Play.Enabled = true;
        }

        private void button_shuffle_Click(object sender, EventArgs e)
        {
            if (state_buttonShuffle)
            {
                button_shuffle.Text = "Shuffle OFF";
                button_shuffle.BackColor = Color.Red;
                state_buttonShuffle = false;

            }
            else
            {
                button_shuffle.Text = "Shuffle ON";
                button_shuffle.BackColor = Color.GreenYellow;
                state_buttonShuffle = true;
            }
        }

        private void button_Play_Click(object sender, EventArgs e)
        {
            MabadumbaPlayer.stop();

            // karısık calma aktifse rastgele bir sarkı baslatılıyor
            if (state_buttonShuffle)
            {
                play_Shuffle();    
            }
            // herhangi bir sarkı secilmemisse birinci sarkı baslatılıyor
            else if (listBox_Song.SelectedItem == null)
            {
                listBox_Song.SelectedIndex = 0;
                play_Selection();
            }
            // secilen sarkı baslatılıyor
            else
            {
                play_Selection();
            }
        }

        private void button_Loop_Click(object sender, EventArgs e)
        {
            if (state_buttonLoop)
            {
                button_Loop.Text = "Loop OFF";
                button_Loop.BackColor = Color.Red;
                state_buttonLoop = false;

            }
            else
            {
                button_Loop.Text = "Loop ON";
                button_Loop.BackColor = Color.GreenYellow;
                state_buttonLoop = true;
            }
        }

        private void listBox_Song_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            play_Selection();
        }

        //////////////////////////////
        //////////////////////////////

        public void play_Selection()
        {

            state_song = 1;

            button_Play.Enabled = true;

            MabadumbaPlayer.stop();

            string song = textBox_URL.Text + "\\" + (string)(listBox_Song.SelectedItem);

            panel_MediaPlayer.URL = song;

            MabadumbaPlayer.open(song);
            MabadumbaPlayer.play(); //(string)listBox_Song.SelectedItem

        }

        public void play_Shuffle()
        {
            state_song = 1;

            button_Play.Enabled = true;
            
            MabadumbaPlayer.stop();

            Random random = new Random();
            int songnumber = random.Next(1, listBox_Song.Items.Count);

            listBox_Song.SelectedIndex = songnumber;

            string song = textBox_URL.Text + "\\" + (string)(listBox_Song.SelectedItem);

            MabadumbaPlayer.open(song);
            MabadumbaPlayer.play(); //(string)listBox_Song.SelectedItem
        }

        public void play_Loop()
        {

        }

        public void checkStatus()
        {
            //label_Status.Text = MabadumbaPlayer.Status();
        }

        private void Mabaduma_FormClosed(object sender, FormClosedEventArgs e)
        {

            Environment.Exit(Environment.ExitCode);

        }

        private void panel_MediaPlayer_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            state_song += 1;
            if (state_song == 3)
            {
                label_Status.Text = "bitti";
               
            }
            else
                label_Status.Text = "basladı";
                state_song = 0;
        }
    }
}
